<?php
class CetakInfoHewan {
    public $daftarElang = array();
    public function tambahElang(Elang $elang){
        $this->daftarElang[] = $elang;
    }

    public $daftarHarimau = array();
    public function tambahHarimau(Harimau $harimau){
        $this->daftarHarimau[] = $harimau;
    }

    public function cetakElang(){
        $str = "Elang <br>";

        foreach($this->daftarElang as $E){
            $str .= "{$E->getInfo()} <br>";
        }

        return $str;
    }

    public function cetakHarimau(){
        $str = "Harimau <br>";

        foreach($this->daftarHarimau as $H){
            $str .= "{$H->getInfo()} <br>";
        }

        return $str;
    }
}