<?php
class Elang {
    use Hewan;
    use Fight;

    public function __construct($nama = "nama",$jumlahKaki = 0,$keahlian = "keahlian",$attackPower = 0, $defencePower = 0){
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfo(){
        $str = "Nama : " . $this->nama . "<br> 
                Darah : ". $this->darah ."<br> 
                Jumlah Kaki : ". $this->jumlahKaki ."<br> 
                Keahlian : ". $this->keahlian ."<br> 
                Attack Power : ". $this->attackPower ."<br> 
                Defence Power : ". $this->defencePower;
        return $str;
    }
}