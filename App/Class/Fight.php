<?php
trait Fight {
    public $attackPower, $defencePower;

    public function serang($lawan){
        return $this->nama." Menyerang ".$lawan."<br>";
    }

    public function diSerang($lawan, $attackPenyerang){
        $this->darah = $this->darah - $attackPenyerang / $this->defencePower;
        return $this->nama. " Sedang Diserang " . $lawan . " Sisa Darah = " . $this->darah;
    }
}