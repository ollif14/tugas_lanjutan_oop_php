<?php

require_once 'App/init.php';

$elang = new Elang("Fiacra", 2, "Terbang Tinggi", 10, 5);
$harimau = new Harimau("Sarkan", 4, "Berlari Cepat", 7, 8);


$cetakHewan = new CetakInfoHewan();
    $cetakHewan->tambahElang($elang);
    $cetakHewan->tambahHarimau($harimau);

    echo "DAFTAR HEWAN : <br> <hr>";
    echo $cetakHewan->cetakElang(). "<br>";
    echo $cetakHewan->cetakHarimau(). "<br>";

    echo "ATRAKSI : <br><hr>";
    echo "Elang ".$elang->atraksi();
    echo "Harimau ".$harimau->atraksi();
    echo "<br>";

    echo "FIGHT : <br>";
    echo "Pertarungan Dengan 20 Ronde<br><hr>";
    $ronde = 1;
    do{
        echo "rounde : ".$ronde."<br>";
        echo $harimau->serang($elang->nama);
        echo $elang->diSerang($harimau->nama, $harimau->attackPower)."<br>";
        echo $elang->serang($harimau->nama);
        echo $harimau->diSerang($elang->nama, $elang->attackPower)."<br>";
        $ronde++;
    } while($ronde<=20);

    echo"<br>";
    echo "HASIL FIGHT : <br><hr>";
    if($elang->darah > $harimau->darah){
        echo "Pemenangnya Adalah : ".$elang->nama."<br>";
    }else if($harimau->darah > $elang->darah){
        echo "Pemenangnya Adalah : ".$harimau->nama."<br>";
    } else {
        echo "Draw";
    }
